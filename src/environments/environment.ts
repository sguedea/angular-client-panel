// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBnuWn3VGlE7Cfk7h2FkStpwvYXfuvlSN8",
    authDomain: "angular-client-2cbe4.firebaseapp.com",
    databaseURL: "https://angular-client-2cbe4.firebaseio.com",
    projectId: "angular-client-2cbe4",
    storageBucket: "angular-client-2cbe4.appspot.com",
    messagingSenderId: "608696906046",
    appId: "1:608696906046:web:be70782e4a86e33cf07c6f",
    measurementId: "G-F7ZP86LC88"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
