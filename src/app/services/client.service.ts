import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from "@angular/fire/firestore";
import { Observable } from "rxjs";

import { Client } from "../models/Client";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  // clients collection for the collection of clients
  // client doc for single documents
  // clients observable to return to a component
  // client observable to return an individual client

  // our properties
  clientsCollection: AngularFirestoreCollection<Client>;
  clientDoc: AngularFirestoreDocument<Client>;
  clients: Observable<Client[]>;
  client: Observable<Client>;


// injection angular firestore as a dependency
  constructor(private afs: AngularFirestore) {
    this.clientsCollection = this.afs.collection('clients', ref => ref.orderBy("lastName", 'asc'));
  }


  getClients(): Observable<Client[]> {
    this.clients = this.clientsCollection.snapshotChanges().pipe(
      map(changes => changes.map(c => {
        const data = c.payload.doc.data() as Client;
        data.id = c.payload.doc.id;

        return data;
      }))
    );
    console.log('hello from getClients');
    return this.clients;
  }

  addNewClient(client: Client) {
    this.clientsCollection.add(client);
  }

  getClient(id: string): Observable<Client> {
    this.clientDoc = this.afs.doc<Client>(`clients/${id}`);
    this.client = this.clientDoc.snapshotChanges().pipe(
      map(action => {
        if (action.payload.exists === false) {
          return null;
        }
        else {
          const data = action.payload.data() as Client;
          data.id = action.payload.id;

          return data;
        }
      })
    );

    return this.client;
  }


  updateClient(client: Client) {
    this.clientDoc = this.afs.doc(`clients/${client.id}`);
    this.clientDoc.update(client);
  }

  deleteClient(client: Client) {
    this.clientDoc = this.afs.doc(`clients/${client.id}`);
    this.clientDoc.delete();
  }
}
