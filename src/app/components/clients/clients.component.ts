import { Component, OnInit } from '@angular/core';
import { ClientService } from "../../services/client.service";

import { Client } from "../../models/Client";


@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent implements OnInit {
  clients: Client[];
  totalOwed: number;

  // dependency injection
  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
    // fetching the clients
    this.clientService.getClients().subscribe(clients => {
      // console.log(clients); works!
      this.clients = clients;

      // calling getTotalOwed()
      this.getTotalOwed();
    });
  }

  getTotalOwed() {
    this.totalOwed = this.clients.reduce((total, client) => {
      return total + parseFloat(client.balance.toString());
    }, 0);

  }

}
